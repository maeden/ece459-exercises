use std::sync::mpsc;
use std::thread;
use std::time::Duration;

static N: i32 = 10;

// You should modify main() to spawn threads and communicate using channels
fn main() {
    let (tx, rx) = mpsc::channel();
    let mut children = vec![];

    for i in 1..N {
        let tx1 = mpsc::Sender::clone(&tx);
        children.push(thread::spawn(move || {
            tx1.send(format!("Thread {}", i)).unwrap();
            thread::sleep(Duration::new(1, 0));
            println!("Thread {}", i);
        }))
    }

    let mut ids = Vec::with_capacity(N as usize);
    for _i in 1..N {
        ids.push(rx.recv())
    }

    for child in children {
        let _ = child.join();
    }

    println!("IDs: {:?}", ids);
}
