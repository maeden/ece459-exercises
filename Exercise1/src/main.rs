// You should implement the following function:

fn sum_of_multiples(number: i32, multiple1: i32, multiple2: i32) -> i32 {
    let mut sum = 0;

    let mut i = multiple1;
    while i < number {
        sum += i;
        i += multiple1;
    }

    i = multiple2;
    while i < number {
        sum += i;
        i += multiple2;
    }

    return sum;
}

fn main() {
    println!("Sum of multiples: {}", sum_of_multiples(1000, 5, 3));
}
