// You should implement the following function
fn fibonacci_number(n: u32) -> u32 {
    let mut a = 0;
    let mut b = 1;
    let mut temp = 0;
    for i in 1..n {
        temp = b;
        b += a;
        a = temp;
    }
    return b;
}


fn main() {
    println!("{}", fibonacci_number(10));
}
